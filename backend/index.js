const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config() // Open file
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')
const connection = mysql.createConnection({
    host : 'localhost',
    user : 'Manager',
    password : 'Manager',
    database : 'ManagementSystem'
})

connection.connect()

const express = require('express')
const app = express()
const port = 4000

/* ===== Middleware for Authentication User Token ======*/
function authenticateToken(req, res, next) {
    const authenHeader = req.headers['authorization']
    const token = authenHeader && authenHeader.split(' ')[1]
    
    if(token == null) {
        return res.sendStatus(401)
    }
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if(err) {
            return res.sendStatus(403)
        }else {
            req.user = user
            next()
        }
    })
}

/* ===== INSERT INTO USERS ===== */
app.post('/insert_category', (req, res) => {
    Name = req.query.Name
    Status = req.query.Status

    query = `INSERT INTO Category(Name, Status)
                         VALUES('${Name}', '${Status}') `
    connection.query( query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({
                        'status' : '400',
                        'message' : 'Error can not insert your name and status'
                    })
        }else {
            res.json({
                'status' : '200',
                'message' : `Add Catrgoty '${Name} ${Status}' succesful`
            })
        }
    })
})

/* ===== SELECT USERS ===== */
app.post('/list_users',(req,res) => {
    query = `SELECT * from Users`
    connection.query(query,(err,rows) => {
        if(err){
            res.json({
                'status' : '400',
                'message' : 'Error querying from users db'
            })
        }else{
            res.json(rows)
        }
    })
})

/* ===== UPDATE USERS ===== */
app.post('/update_users', authenticateToken, (req,res) => {
    UserID = req.query.UserID
    Name = req.query.Name
    Surname = req.query.Surname
    
    query = `UPDATE Users SET
                    Name = '${Name}',
                    Surname = '${Surname}'
                    WHERE UserID = ${UserID}`

    console.log(query)

    connection.query(query,(err,rows) =>{
        console.log(err)
        if (err){
            res.json({
                'status' : '400',
                'message' : `Error can't update users`
            })
        }else{
            res.json({
                'status' : '200',
                'message' : `Updating ${UserID} succesful`
            })
        }
    })
})

/* ===== DELETE USERS ===== */
app.post('/delete_users', authenticateToken ,(req, res) => {
    UserID = req.query.UserID

    query = `DELETE FROM Users WHERE UserID = ${UserID}`
    console.log(query)

    connection.query(query, (err, rows) => {
        if(err) {
            console.log(err)
            res.json({ 
                        'status' : '400',
                        'message' : 'Error deleting record'
                     })
        }else {
            res.json({ 
                'status' : '200',
                'message' : 'Deleting record success'
             })
        }
    })
})

/* ===== REGISTER USERS ===== */
app.post("/register_users", (req,res) => {
    UserName = req.query.UserName
    UserSurname = req.query.UserSurname
    UserUsername = req.query.UserUsername
    UserPassword = req.query.UserPassword

    bcrypt.hash(UserPassword, SALT_ROUNDS, (err, hash) => {
        let query = `INSERT INTO Users (Name, Surname, Username, Password, IsAdmin) 
                     VALUES ('${UserName}', 
                             '${UserSurname}',
                             '${UserUsername}',
                             '${hash}', false)`
        console.log(query)

        connection.query(query, (err, rows) => {
            if(err) {
                console.log(err)
                res.json({
                    'status' : '400',
                    'message' : 'Error inserting data into db'
                })
            }else {
                res.json({ 
                    'status' : '200',
                    'message' : 'Adding new user successful'
                })
            }
        })
    })
})

/* ===== LOGIN USERS ===== */
app.post("/login_users", (req, res) =>{
    Username = req.query.Username
    UserPassword = req.query.UserPassword

    query = `SELECT * FROM Users WHERE Username = '${Username}'`
    connection.query(query, (err, rows) => {
        if(err) {
            res.json({
                "status" : "400",
                "message" : "Error querying from users db"
            })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(UserPassword, db_password, (err, result) => {
                if(result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].RunnerID,
                        "IsAdmin" : rows[0].IsAdmin
                    }
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d' })
                    res.send(token)
                }else {
                    res.send("Invalid username / password")
                }
            })
        }
    })
})

app.listen(port, () => {
    console.log(`Start now backend ${port}`)
})

/*
query = 'SELECT * from User'
connection.query( query, (err, rows) => {
    if(err) {
        console.log(err)
    }else {
        console.log(rows)
    }
})

connection.end()
*/